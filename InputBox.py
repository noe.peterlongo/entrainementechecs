import pygame as pg

class InputBox():
    def __init__(self, rect: pg.Rect) -> None:
        self.rect = rect
        self.text = "Tset"
        self.selected = False

    def render(self, screen):
        if self.selected:
            pg.draw.rect(screen, (150,150,150), self.rect.inflate(1.2, 1.2))
            pg.draw.rect(screen, (60, 60, 100), self.rect)
        else:
            pg.draw.rect(screen, (60, 60, 60), self.rect.inflate(1.2, 1.2))

        font = pg.font.SysFont("Arial", 25)
        textX = self.rect.left + 5
        textY = self.rect.top + self.rect.height/2 - font.size("A")[1]/2
        screen.blit(font.render(self.text, True, (255, 255, 255)), (textX, textY))

    def checkEvent(self, event: pg.event.Event):
        if event.type == pg.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.selected = True
            else:
                self.selected = False

        if self.selected and event.type == pg.KEYDOWN:
            if event.key == pg.K_BACKSPACE:
                if len(self.text) > 0:
                    self.text = self.text[:-1]
            elif event.key == pg.K_DELETE:
                self.text = ""
            else:
                self.text += event.unicode

    