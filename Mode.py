from typing import List, Tuple
import pickle

class Mode():
    def __init__(self, board: List[List[str]], colorBoard: List[List[Tuple]]) -> None:
        self.board = board
        self.colorBoard = colorBoard
        self._loadDict()

    def _loadDict(self) -> None:
        self.dictionnary = pickle.load(open("dictionnary.pckl", "rb"))

    def _saveDict(self) -> None:
        pickle.dump(self.dictionnary, open("dictionnary.pckl", "wb"))

    def _move(self, origin: Tuple[int, int], destination: Tuple[int, int]):
        self.board[destination[0]][destination[1]] = self.board[origin[0]][origin[1]]
        self.board[origin[0]][origin[1]] = ""
