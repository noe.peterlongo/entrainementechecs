from typing import List, Tuple
import random
import sys

from utils import *
from Mode import Mode

class NormalMode(Mode):

    def __init__(self, board: List[List[str]], colorBoard: List[List[Tuple]]) -> None:
        super().__init__(board, colorBoard)
        self.colorPlayed = "White"
        self.whiteSequences = []
        self.blackSequences = []
        for key, value in self.dictionnary.items():
            if value[0] == "White":
                self.whiteSequences.append(value[1:])
            else:
                self.blackSequences.append(value[1:])
        
        if len(self.whiteSequences) == 0:
            print("Error: no white sequence")
            sys.exit()

        self.currentSequence = []
        self.index = 0
        self.reinit()

    def reinit(self) -> None:
        initBoard(self.board)
        for i in range(8):
            for j in range(8):
                self.colorBoard[i][j] = (0, 0, 0, 0)

        # Select random sequence
        if self.colorPlayed == "White":
            index = random.randint(0, len(self.whiteSequences) - 1)
            self.currentSequence = self.whiteSequences[index]
        else:
            index = random.randint(0, len(self.blackSequences) - 1)
            self.currentSequence = self.blackSequences[index]

        self.playStartSequence(self.currentSequence[0])
        self.index = 0
        print("Next expected move: " + positionToHellsNotation(self.currentSequence[1][self.index][0]) + " " + positionToHellsNotation(self.currentSequence[1][self.index][1]))

        self.selectedPiece = (-1, -1)
        self.endGame = False

    def playStartSequence(self, sequence: List[Tuple[Tuple[int, int]]]):
        for move in sequence:
            self._move(move[0], move[1])

    def clickCase(self, l: int, c: int) -> str:

        if self.endGame:
            return ""

        if self.selectedPiece == (-1, -1): # No piece selected
            # Only a playedColor can be selected
            if self.board[l][c][:5] == self.colorPlayed:
                self.selectedPiece = (l, c)
                self.colorBoard[l][c] = (0, 0, 200, 100)
        
        else: # a piece is already selected
            origin = self.selectedPiece

            if origin == (l, c): # Re-click on same piece, to cancel
                self.colorBoard[origin[0]][origin[1]] = (0, 0, 0, 0)
                self.selectedPiece = (-1, -1)
                return ""

            # Check move
            if (origin, (l, c)) == self.currentSequence[1][self.index]: # Good move
                isCastling = self.board[origin[0]][origin[1]][5:] == "King" and abs(origin[1] - c) == 2

                self._move(origin, (l, c))

                if isCastling and len(self.board[l][c]) > 5:
                    row = 7 if self.board[l][c][:5] == "White" else 0

                    if origin[1] > c: # Castling long, to the left
                        self._move((row, 0), (row, 3))
                    else: # Castling short
                        self._move((row, 7), (row, 5))

                self.colorBoard[origin[0]][origin[1]] = (0, 0, 0, 0)
                self.index += 2
                if self.index >= len(self.currentSequence[1]): # End game
                    self.endGame = True
                    if self.index == len(self.currentSequence[1]): # One last move
                        self._move(self.currentSequence[1][self.index - 1][0], self.currentSequence[1][self.index - 1][1])
                    return self.currentSequence[2]
                else:
                    self._move(self.currentSequence[1][self.index - 1][0], self.currentSequence[1][self.index - 1][1])
                    print("Next expected move: " + positionToHellsNotation(self.currentSequence[1][self.index][0]) + " " + positionToHellsNotation(self.currentSequence[1][self.index][1]))
                    self.selectedPiece = (-1, -1)

            else: # Bad move
                
                # Is this move good in another sequence ?
                sequenceList = self.whiteSequences if self.colorPlayed == "White" else self.blackSequences
                found = False
                for sequence in sequenceList:
                    brk = False

                    if len(sequence[1]) <= self.index:
                        break

                    for moveA, moveB in zip(self.currentSequence[0], sequence[0]):
                        if moveA != moveB:
                            brk = True
                            break
                    if brk:
                        break
                    
                    # Same Start sequence as current sequence, let'continue
                    for i in range(self.index):
                        if sequence[1][i] != self.currentSequence[1][i]:
                            brk = True
                            break
                    
                    if brk:
                        break

                    # Is the current move the good one for sequence ?
                    if (origin, (l, c)) == sequence[1][self.index]:
                        self.currentSequence = sequence # change sequence
                        print("Switch opening")
                        return self.clickCase(l, c) # re-do move

                self._move(origin, (l, c))

                print("Bad move")
                print("Expected move: " + positionToHellsNotation(self.currentSequence[1][self.index][0]) + " " + positionToHellsNotation(self.currentSequence[1][self.index][1]))
                self.endGame = True # Ajouter la possibilité de continuer
                self.colorBoard[l][c] = (200, 0, 0, 100)
                self.colorBoard[self.currentSequence[1][self.index][0][0]][self.currentSequence[1][self.index][0][1]] = (0, 200, 0, 100)
                self.colorBoard[self.currentSequence[1][self.index][1][0]][self.currentSequence[1][self.index][1][1]] = (0, 200, 0, 100)

                return "Bad move. " "Expected move: " + positionToHellsNotation(self.currentSequence[1][self.index][0]) + " " + positionToHellsNotation(self.currentSequence[1][self.index][1])
        
        return ""
