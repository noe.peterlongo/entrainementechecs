import os, sys
import pygame as pg
from InputMode import InputMode
from NormalMode import NormalMode
from utils import *
from InputBox import InputBox
from Button import Button
from ScreenBoard import ScreenBoard

# Mode
mode = "normal"
if len(sys.argv) > 1:
    mode = sys.argv[1]
if mode not in ["input", "normal"]:
    print("Mode error " + mode)
    sys.exit()

# Pygame init
pg.init()

displayStep = 50
screenWidth, screenHeight = displayStep*10, displayStep*12

screen = pg.display.set_mode((screenWidth, screenHeight))

# Load Pieces
pieces = dict()
directory = "images/cheq"
for filename in os.listdir(directory):
    f = os.path.join(directory, filename)
    # checking if it is a file
    if os.path.isfile(f):
        name = filename.split('.')[0]
        image = pg.image.load(f)
        image = pg.transform.scale(image, (displayStep, displayStep))
        pieces[name] = image

# Initialization
board = [["" for i in range(8)] for j in range(8)]
colorBoard = [[(0, 0, 0, 0) for i in range(8)] for j in range(8)]
colorPlayed = "White"
screenBoard = ScreenBoard()
initScreen(screen, displayStep, colorPlayed)
pg.time.set_timer(pg.USEREVENT, 40)


if mode == "input":
    ######################      INPUT   MODE    #########################
    # Prepare Interface
    inputBoxName = InputBox(pg.Rect(0.2*displayStep, 10*displayStep, 4*displayStep, 0.6*displayStep))
    inputBoxComment = InputBox(pg.Rect(3*displayStep, 11*displayStep, 6.5*displayStep, 0.6*displayStep))
    inputBoxComment.text = "comment"
    buttonSave = Button(pg.Rect(4.5*displayStep, 10*displayStep, displayStep, 0.6*displayStep), "Save")
    buttonSwitchSequence = Button(pg.Rect(5.8*displayStep, 10*displayStep, 2.8*displayStep, 0.6*displayStep), "Start")
    buttonSwitchBW = Button(pg.Rect(0.1*displayStep, 11*displayStep, 2.5 * displayStep, 0.6*displayStep), "Switch color")
    buttonBack = Button(pg.Rect(8.8*displayStep, 10*displayStep, 1 * displayStep, 0.6*displayStep), "<-")

    gameManager = InputMode(board, colorBoard)

    while True:
        events = pg.event.get()
        for event in events:
            if event.type == pg.QUIT:
                pg.display.quit()
                sys.exit()
            
            if event.type == pg.MOUSEBUTTONUP:
                x, y = pg.mouse.get_pos()
                
                if x > displayStep and x < 9*displayStep and y > displayStep and y < 9*displayStep: # click on board
                    l = (y-displayStep) // displayStep
                    c = (x-displayStep) // displayStep
                    if colorPlayed == "Black":
                        l = 7 - l
                        c = 7 - c
                    gameManager.clickCase(l, c)

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    gameManager.reinit()
                    buttonSwitchSequence.text = "Start"

            if event.type == pg.USEREVENT:
                screenBoard.render(board, colorBoard, screen, displayStep, pieces, colorPlayed)

            inputBoxName.checkEvent(event)
            inputBoxComment.checkEvent(event)

            if buttonSwitchSequence.isClicked(event):
                if gameManager.switchToInteractiveSequence(colorPlayed):
                    buttonSwitchSequence.text = "Interactive"
            
            if buttonSave.isClicked(event):
                gameManager.save(inputBoxName.text, inputBoxComment.text, colorPlayed)

            if buttonSwitchBW.isClicked(event):
                colorPlayed = "White" if colorPlayed == "Black" else "Black"
                initScreen(screen, displayStep, colorPlayed)

            if buttonBack.isClicked(event):
                buttonSwitchSequence.text = gameManager.back()


        # Re-render everything
        if len(events) > 0:
            inputBoxName.render(screen)
            buttonSwitchSequence.render(screen)
            buttonSave.render(screen)
            buttonSwitchBW.render(screen)
            inputBoxComment.render(screen)
            buttonBack.render(screen)
            pg.display.update()

        
else:
    ######################      NORMAL   MODE    #########################

    displayedText = ""
    def printText():
        pg.draw.rect(screen, (0, 0, 0), pg.Rect(3, 10*displayStep, 10*displayStep-6, 0.9*displayStep))
        pg.draw.rect(screen, (20, 20, 25), pg.Rect(3+1, 10*displayStep+1, 10*displayStep-6-2, 0.9*displayStep-2))
        font = pg.font.SysFont("Arial", 20)
        x = screen.get_rect().width / 2 - font.size(displayedText)[0]/2
        y = 10.5*displayStep - font.size(displayedText)[1]/2
        screen.blit(font.render(displayedText, True, (200, 200, 200)), (x, y))
    buttonSwitchBW = Button(pg.Rect(0.1*displayStep, 11*displayStep, 2.5 * displayStep, 0.6*displayStep), "Switch color")

    gameManager = NormalMode(board, colorBoard)

    while True:
        events = pg.event.get()
        for event in events:
            if event.type == pg.QUIT:
                pg.display.quit()
                sys.exit()

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    gameManager.reinit()
                    displayedText = ""

            if event.type == pg.MOUSEBUTTONUP:
                x, y = pg.mouse.get_pos()
                
                if x > displayStep and x < 9*displayStep and y > displayStep and y < 9*displayStep: # click on board
                    l = (y-displayStep) // displayStep
                    c = (x-displayStep) // displayStep
                    if colorPlayed == "Black":
                        l = 7 - l
                        c = 7 - c
                    text = gameManager.clickCase(l, c)
                    if text != "":
                        displayedText = text
            
            if event.type == pg.USEREVENT:
                screenBoard.render(board, colorBoard, screen, displayStep, pieces, colorPlayed)

            if buttonSwitchBW.isClicked(event):
                colorPlayed = "White" if colorPlayed == "Black" else "Black"
                initScreen(screen, displayStep, colorPlayed)
                gameManager.colorPlayed = colorPlayed
                gameManager.reinit()

        # Re-render everything
        if len(events) > 0:
            printText()
            buttonSwitchBW.render(screen)
            pg.display.update()
