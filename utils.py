from typing import Tuple
import pygame as pg

letterToColumn = {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8}
indexToLetter = ["a", "b", "c", "d", "e", "f", "g", "h"]

def positionToHellsNotation(position: Tuple[int, int]) -> str:
    return indexToLetter[position[1]] + str(8 - position[0])

def initScreen(screen: pg.Surface, displayStep:float, colorPlayed:str):
    pg.draw.rect(screen, (30, 30, 35), pg.Rect(0, 0, screen.get_width(), screen.get_height()))

    font = pg.font.SysFont("Arial", 30)

    # Draw coordinates
    i = 0
    letters = ["a", "b", "c", "d", "e", "f", "g", "h"]
    if colorPlayed == "Black":
        letters.reverse()
    for letter in letters:
        x = displayStep + i * displayStep + displayStep/2 - font.size(letter)[0]/2
        y1 = displayStep/2 - font.size(letter)[1]/2
        y2 = displayStep*9.5 - font.size(letter)[1]/2
        screen.blit(font.render(letter, True, (255, 255, 255)), (x, y1))
        screen.blit(font.render(letter, True, (255, 255, 255)), (x, y2))
        i += 1

    for nb in range(8):
        x1 = displayStep/2 - font.size("0")[0]/2
        x2 = displayStep*9.5 - font.size("0")[0]/2
        y = (nb + 1.5) * displayStep - font.size("0")[1]/2

        if colorPlayed == "White":
            txt = str(8 - nb)
        else:
            txt = str(nb + 1)

        screen.blit(font.render(txt, True, (255, 255, 255)), (x1, y))
        screen.blit(font.render(txt, True, (255, 255, 255)), (x2, y))
            
def initBoard(board):
    for i in range(8):
            for j in range(8):
                board[i][j] = ""
    for i in range(8):
        board[1][i] = "BlackPawn"
        board[6][i] = "WhitePawn"
    board[0][0] = "BlackRook"
    board[0][7] = "BlackRook"
    board[7][0] = "WhiteRook"
    board[7][7] = "WhiteRook"
    board[0][1] = "BlackKnight"
    board[0][6] = "BlackKnight"
    board[7][1] = "WhiteKnight"
    board[7][6] = "WhiteKnight"
    board[0][2] = "BlackBishop"
    board[0][5] = "BlackBishop"
    board[7][2] = "WhiteBishop"
    board[7][5] = "WhiteBishop"
    board[0][3] = "BlackQueen"
    board[0][4] = "BlackKing"
    board[7][3] = "WhiteQueen"
    board[7][4] = "WhiteKing"

