from typing import List, Tuple

from utils import *
from Mode import Mode

class InputMode(Mode):

    def __init__(self, board: List[List[str]], colorBoard: List[List[Tuple]]) -> None:
        super().__init__(board, colorBoard)
        self.reinit()

    def reinit(self) -> None:
        self.startSequence = []
        self.interactiveSequence = []
        self.activeSequence = "Start"
        self.selectedPiece = (-1, -1)
        self.whiteTurn = True
        initBoard(self.board)
        for i in range(8):
            for j in range(8):
                self.colorBoard[i][j] = (0, 0, 0, 0)

    def clickCase(self, l: int, c: int) -> None:
        
        if self.selectedPiece != (-1, -1): # a piece is already selected
            if self.selectedPiece == (l, c): # Re-click on same piece, to cancel
                self.selectedPiece = (-1, -1)
            else:
                isCastling = self.board[self.selectedPiece[0]][self.selectedPiece[1]][5:] == "King" and abs(self.selectedPiece[1] - c) == 2

                self._move(self.selectedPiece, (l, c))

                if isCastling and len(self.board[l][c]) > 5:
                    row = 7 if self.board[l][c][:5] == "White" else 0

                    if self.selectedPiece[1] > c: # Castling long, to the left
                        self._move((row, 0), (row, 3))
                    else: # Castling short
                        self._move((row, 7), (row, 5))


                if self.activeSequence == "Start":
                    self.startSequence.append(((self.selectedPiece), (l, c)))
                else:
                    self.interactiveSequence.append(((self.selectedPiece), (l, c)))
                self.selectedPiece = (-1, -1)
                self.whiteTurn = not self.whiteTurn
        else: # No piece selected
            if self.board[l][c] != "": # a piece was clicked
                if (self.board[l][c][:5] == "White" and self.whiteTurn) or (self.board[l][c][:5] == "Black" and not self.whiteTurn): # Good color
                    self.selectedPiece = (l, c)

        # Update colorBoard
        for i in range(8):
            for j in range(8):
                if (i, j) == self.selectedPiece:
                    self.colorBoard[i][j] = (0, 0, 200, 100) if self.activeSequence == "Interactive" else (0, 200, 0, 100)
                else:
                    self.colorBoard[i][j] = (0, 0, 0, 0)

    def switchToInteractiveSequence(self, colorPlayed: str) -> bool:
        if (colorPlayed == "White" and len(self.startSequence)%2 == 0) or (colorPlayed == "Black" and len(self.startSequence)%2 == 1):
            self.activeSequence = "Interactive"
            return True
        return False
    
    def save(self, name, comment, colorPlayed):
        self.dictionnary[name] = [colorPlayed, [t for t in self.startSequence], [t for t in self.interactiveSequence], comment]
        print("Saved '"+ name+ "'", self.dictionnary[name])
        self._saveDict()

    def back(self) -> str:
        isCastling = False

        if self.activeSequence == "Interactive":
            if len(self.interactiveSequence) == 0: # No move back, but changes sequence
                self.activeSequence = "Start"
                return self.activeSequence
            
            move = self.interactiveSequence.pop()
            self._move(move[1], move[0])
            isCastling = self.board[move[0][0]][move[0][1]][5:] == "King" and abs(move[0][1] - move[1][1]) == 2

        else:
            if len(self.startSequence) > 0:
                move = self.startSequence.pop()
                self._move(move[1], move[0])
                isCastling = self.board[move[0][0]][move[0][1]][5:] == "King" and abs(move[0][1] - move[1][1]) == 2
        
        if isCastling:
            origin, destination = move
            row = 7 if self.board[origin[0]][origin[1]][:5] == "White" else 0

            if destination[1] < origin[1]: # Castling long, to the left
                self._move((row, 3), (row, 0))
            else: # Castling short
                self._move((row, 5), (row, 7))


        self.selectedPiece = (-1, -1)
        self.whiteTurn = not self.whiteTurn
        for i in range(8):
            for j in range(8):
                self.colorBoard[i][j] = (0, 0, 0, 0)

        return self.activeSequence    

