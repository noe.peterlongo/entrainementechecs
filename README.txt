Mode normal:
python3 ouvertures.py

Mode saisie:
python3 ouvertures.py input


Les ouvertures sont divisées en deux parties:
    - Start sequence, pour initialiser le plateau
    - Interactive sequence, où le joueur euh... joue
        cette séquence commence par un mouvement du joueur, quelque soit sa couleur

Mode saisie:
    - Echap pour reinitialiser
    - On saisit la séquence d'initialisation
    - On clique sur "Start sequence" pour passer en mode "Interactive sequence"
    - On saisit la séquence interactive, dans l'ordre, en commençant par la couleur du joueur
    La couleur du joueur est définie par l'orientation du plateau
    - On entre un nom pour identifier l'ouverture
    - Un commentaire
    - Save
    - vérifier dans la console

Mode normal:
    - Selection de la couleur
    - Echap pour reinitialiser, et changer (peut-être) de séquence

Suppr pour supprimer tout le texte

A faire:
    - En cas de mauvais mauvais mouvement, donner la possibilité de continuer
    - Bouton retour
    - Eviter si possible de reprendre la même ouverture ?