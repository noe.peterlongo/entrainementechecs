from typing import List, Tuple
import pygame as pg
import numpy as np

speed = 0.8
def caseColor(l, c):
    return [235, 236, 208] if l%2 == c%2 else [119, 149, 86]

class ScreenPiece():
    def __init__(self, name, position) -> None:
        self.name = name
        self.position = np.array(position, dtype=float) # Displayed position
        self._target = np.array(position, dtype=float) # True position

    def step(self):
        distance = np.linalg.norm(self._target - self.position)
        if distance < speed:
            self.position = np.copy(self._target)
            return
        direction = (self._target - self.position) / distance

        self.position += direction * speed

    def getTarget(self):
        return (int(self._target[0]), int(self._target[1]))
    
    def setTarget(self, target: Tuple[int, int]):
        self._target[0], self._target[1] = target

class ScreenBoard():
    def __init__(self) -> None:
        self.screenPieces : List[ScreenPiece] = [] # Probably useless
        self.lastBoard = None
    
    def _updateBoard(self, board: List[List[str]]):
        if self.lastBoard == None:
            self._setBoard(board) # initialization
            return

        missingPiecesPositions = []
        newPiecesPositions = []

        for i in range(8):
            for j in range(8):
                # Missing pieces
                if board[i][j] != self.lastBoard[i][j]:
                    missingPiecesPositions.append((i, j))
                # New pieces
                if board[i][j] != self.lastBoard[i][j] and board[i][j] != "":
                    newPiecesPositions.append((i, j))

        # Find moved pieces
        for newPosition in newPiecesPositions:
            pieceName = board[newPosition[0]][newPosition[1]]

            # Find this piece in missingpieces
            oldPosition = None
            for missingPiecePosition in missingPiecesPositions:
                if self.lastBoard[missingPiecePosition[0]][missingPiecePosition[1]] == pieceName:
                    oldPosition = missingPiecePosition
                    break
            
            if oldPosition == None: # Piece apparition, no animation
                self._setBoard(board) # re - initialization
                return

            # Find corresponding screen piece object
            found = False
            for screenPiece in self.screenPieces:
                if screenPiece.getTarget() == oldPosition:
                    screenPiece.setTarget(newPosition)
                    found = True
                    break
            if not found:
                print("Error screen piece not found")
            
            missingPiecesPositions.remove(oldPosition)
                
        # Find disappeared pieces
        for missingPiecePosition in missingPiecesPositions: # maybe useless
            for screenPiece in self.screenPieces:
                if screenPiece.getTarget() == missingPiecePosition and board[missingPiecePosition[0]][missingPiecePosition[1]] != screenPiece.name:
                    self.screenPieces.remove(screenPiece)

        # Update lastBoard
        self.lastBoard = [[board[i][j] for j in range(8)] for i in range(8)]

    def _setBoard(self, board: List[List[str]]):
        self.lastBoard = [[board[i][j] for j in range(8)] for i in range(8)]

        self.screenPieces = []
        for i in range(8):
            for j in range(8):
                if board[i][j] != "":
                    self.screenPieces.append(ScreenPiece(board[i][j], (i, j)))

    def render(self, board, colorBoard, screen: pg.Surface, displayStep, piecesImages, colorPlayed):
        self._updateBoard(board)

        # Draw chessBoard
        for l in range(8):
            for c in range(8):
                if colorPlayed == "White":
                    i, j = l, c
                else:
                    i, j = 7 - l, 7 - c

                color = caseColor(l, c)
                for k in range(3):
                    color[k] = int((color[k]*(255 - colorBoard[i][j][3]) + colorBoard[i][j][k]*colorBoard[i][j][3])/255)

                pg.draw.rect(screen, color, pg.Rect((c+1)*displayStep, (l+1)*displayStep, displayStep, displayStep))
        
        # Draw pieces
        for piece in self.screenPieces:
            piece.step()
            position = piece.position
            if colorPlayed == "Black":
                position = 7 - position
            position = piece.position*displayStep + displayStep
            screen.blit(piecesImages[piece.name], (position[1], position[0]))


