import pygame as pg

class Button():
    def __init__(self, rect: pg.Rect, text) -> None:
        self.rect = rect
        self.text = text
        self.isBeeingClicked = False # Display stuff

    def render(self, screen):
        color = (25, 25, 25) if self.isBeeingClicked else (20, 20, 20)

        if self.isBeeingClicked:
            pg.draw.rect(screen, (50, 50, 60), self.rect.inflate(3, 3))
        else:
            pg.draw.rect(screen, (30, 30, 35), self.rect.inflate(3, 3))
        
        mainRect = self.rect if not self.isBeeingClicked else self.rect.move(1,1)

        pg.draw.rect(screen, color, mainRect)
        font = pg.font.SysFont("Arial", 25)
        textX = mainRect.left + mainRect.width/2 - font.size(self.text)[0]/2
        textY = mainRect.top + mainRect.height/2 - font.size("A")[1]/2
        screen.blit(font.render(self.text, True, (255, 255, 255)), (textX, textY))

    def isClicked(self, event: pg.event.Event) -> bool:
        if event.type == pg.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(event.pos):
                self.isBeeingClicked = True
                return True
            
        # Display stuff
        if event.type == pg.MOUSEBUTTONUP:
            self.isBeeingClicked = False
        if event.type == pg.MOUSEMOTION:
            if not self.rect.collidepoint(event.pos):
                self.isBeeingClicked = False

        return False
